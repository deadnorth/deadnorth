![MasterHead](https://64.media.tumblr.com/ca828825e1c2b7158ba4bd4419ea3887/d22525b4c14f4631-44/s640x960/8cad8a9ec2b977b5ed18a6ab009026d2511f0a81.gif)
<h1 align="center">Hi 👋, I'm deadnorth</h1>
<h3 align="center">A passionate junior developer from Turkey</h3>
<img align="right" alt="Coding" width="400" src="https://64.media.tumblr.com/241c75d2922c780f0a147205f6a184f8/8db22dbf6df3abe8-10/s640x960/231d6a6575ed48061e2de51ad46154b51b626f2e.gif">

- 🌱 I’m currently learning **Python and C**

- 🎬 I love watching animes & films 

- 🎮 I play **Minecraft, VALORANT, Genshin & Hyenas Closed Alpha**

- 📚 I read mangas, novels & comics

- 📝 I write articles on [https://www.discordhaber.com/](https://www.discordhaber.com/)

- 📫 Contact **mali.20072009@outlook.com**


<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://twitter.com/sercee_xyz" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="sercee_xyz" height="30" width="40" /></a>
<a href="https://instagram.com/sercee.xyz" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="sercee.xyz" height="30" width="40" /></a>
<a href="https://discord.com/users/971120135656058901" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/discord.svg" alt="serçe#1234" height="30" width="40" /></a>
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.blender.org/" target="_blank" rel="noreferrer"> <img src="https://64.media.tumblr.com/4501f347582301b01fe18744b8dca99b/a1375fd41ffc3e21-a6/s2048x3072/02800128c97211212ac2bb982951987c17ca2c6b.pnj" alt="blender" width="40" height="40"/> </a> <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> <a href="https://www.figma.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://unity.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/unity3d/unity3d-icon.svg" alt="unity" width="40" height="40"/> </a> </p>



